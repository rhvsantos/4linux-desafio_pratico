#!/bin/bash

url="http://4linuxapp.rafasants.com:8000"
_sabores=(banana nata nutella feijão 'leite desnatado' açaí agrião berinjela \
limão abacate abacaxi 'leite condensado' normal 'não gosto de pudim' feijoada \
'leite ninho' 'não sei' teste laranja branco marrom verde amarelo teste123 argentino \
'sem açucar' goiaba 'dois amores' napolitano creme 'doce de leite' chocolate )

while true
do
	for flavour in "${_sabores[@]}"
	do
    		curl -s -X POST -d "flavour=$flavour" "$url"
	done
done

exit 0

